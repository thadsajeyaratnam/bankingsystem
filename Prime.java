class Prime
{
public void isPrime(int n) {
    if(n % 2 == 0) {
       System.out.println("It is not a prime number");
       return;
    }   
    else {
       int i = 3;
       while(i <= Math.sqrt(n) ) {
           if( (n % i) == 0) {
              System.out.println("It is not a prime number");
              return;
           }
           i = i + 2; 
       }
    }
    System.out.println("It is a prime number");
    return;
}
}